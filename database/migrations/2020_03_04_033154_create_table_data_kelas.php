<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDataKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->bigInteger('kelas_id')->unsigned()->nullable();
            $table->foreign('kelas_id','fk_kelas')->references('id')->on('master_kelas')->onDelete('cascade');
            $table->bigInteger('variable_id')->unsigned()->nullable();
            $table->foreign('variable_id','fk_variable')->references('id')->on('variable')->onDelete('cascade');
            $table->bigInteger('tahun_id')->unsigned()->nullable();
            $table->foreign('tahun_id','fk_tahun')->references('id')->on('tahun')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas');
    }
}
