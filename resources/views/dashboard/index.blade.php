@extends('layout.app')
@section('title','Dashboard')
@section('Dashboard','active')
@section('content')
<!-- SOCIAL -->
<div class="dashboard-section no-margin">
    <div class="section-heading clearfix">
        <h2 class="section-title"><i class="fa fa-pie-chart"></i> Data Sekolah <span class="section-subtitle">(last report)</span></h2>
    </div>
    <div class="panel-content">
        <div class="row">
            <a href="/guru">
            <div class="col-md-3 col-sm-6">
                <p class="metric-inline"><i class="fa fa-mortar-board"></i> {{$guru}} <span>GURU</span></p>
            </div>
            </a>
            <a href="/siswa">
            <div class="col-md-3 col-sm-6">
                <p class="metric-inline"><i class="fa fa-user"></i> {{$siswa}} <span>SISWA</span></p>
            </div>
            </a>
            <a href="/kelas">
            <div class="col-md-3 col-sm-6">
                <p class="metric-inline"><i class="fa fa-building"></i> {{$kelas*$variable}} <span>KELAS</span></p>
            </div>
            </a>
            <a href="/pelajaran">
            <div class="col-md-3 col-sm-6">
                <p class="metric-inline"><i class="fa fa-book"></i> {{$pelajaran}} <span>PELAJARAN</span></p>
            </div>
            </a>
        </div>
    </div>
</div>
<!-- END SOCIAL -->
@stop