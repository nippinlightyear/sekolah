@extends('layout.app')
@section('title', 'Data Kelas')
@section('Data', 'active')
@section('Data.Kelas', 'active')
@section('content')

<div class="section-heading clearfix">
    <h2 class="section-title"><i class="fa fa-pie-chart"></i> Data Kelas </h2>
</div>
<div class="panel-content">
@if(in_array(auth()->user()->role,[1,2]))
    <a href="#" role="modal" onclick="tambahKelas()" class="btn btn-primary btn-sm right" ><i class="lnr lnr-plus-circle"></i></a>
@endif
  <table class="table table-hover" id="TableKelas">
    <thead>
        <tr>
            <th>Kelas</th>
            <th>Tahun</th>
            @if(in_array(auth()->user()->role,[1,2]))
            <th>Option</th>
            @endif
        </tr>
    </thead>
    <tbody>
      @foreach($data_kelas as $kelas)
      <tr>
          <td>{{$kelas->kelas->kelas}} {{$kelas->variable->variable}}</td>
          <td>{{$kelas->tahunpel->tahun}}</td>
          @if(in_array(auth()->user()->role,[1,2]))
          <td><a href="#" onclick="updateKelas({{$kelas->id}})" class="btn btn-warning btn-xs">
                <span class="sr-only">Update</span>
                <i class="fa fa-pencil"></i>
              </a>
              <a href="#" onclick="deleteKelas({{$kelas->id}})" class="btn btn-danger btn-xs">
                <span class="sr-only">Update</span>
                <i class="fa fa-trash-o"></i>
              </a>
          </td>
          @endif
        </tr>
    @endforeach
    </tbody>
  </table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalKelas" tabindex="-1" role="dialog" aria-labelledby="TahunModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalTahun">Tambah Data Tahun</h5>
            </div>
            <form id="FormKelas">
            <div class="modal-body">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Kelas</label>
                        <select name="kelas_id" id="kelas" class="form-control">
                            @foreach($master_kelas as $kls)
                            <option value="{{$kls->id}}">{{$kls->kelas}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Variable</label>
                        <select name="variable_id" id="variable" class="form-control">
                            @foreach($variable as $var)
                            <option value="{{$var->id}}">{{$var->variable}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Tahun</label>
                        <select name="tahun_id" id="tahun" class="form-control">
                            @foreach($tahunpel as $tahun)
                            <option value="{{$tahun->id}}">{{$tahun->tahun}}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-simpan">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endModal -->
@stop
@push('scripts')
<script>
var formAction, table, uid
var method = 'POST'
var ModalKelas = $('#modalKelas')
var FormKelas = $('#FormKelas')
var resetform = function(){
    method = 'POST'
    $('[name="kelas_id"]').val(''),
    $('[name="variable_id"]').val(''),
    $('[name="tahun_id"]').val('')
}
var done = function(resp){
    ModalKelas.modal('hide'),
    window.location.reload()
}
var tambahKelas = function(){
    formAction = '{{ route('create.kelas') }}',
    resetform(),
    ModalKelas.modal('show')
}
var updateKelas = function(id){
    $.ajax({
        url: 'kelas/'+id+'/edit',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        type: 'GET',
        data: { id: id },
        dataType: 'JSON',
        success: function(resp){
            formAction = 'kelas/'+id+'/update',
            method = 'PUT'

            var data = resp.data;
            $('[name="kelas_id"]').val(data.kelas_id),
            $('[name="variable_id"]').val(data.variable_id),
            $('[name="tahun_id"]').val(data.tahun_id),
            ModalKelas.modal('show')
        }
    })
}
$(function(){
    $('.btn-simpan').click(function(e){
        e.preventDefault()
        var data = FormKelas.serialize()
        $.ajax({
            url: formAction,
            method: method,
            data: data,
            dataType: 'JSON',
            success:function(resp){
                done(resp);
            }
        })
    })
})
var deleteKelas = function(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            url: 'kelas/'+id+'/delete',
            type: 'DELETE',
            data: {id: id},
            dataType: 'JSON',
        })
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success',
        $.ajax({
            success: function(resp){
                done(resp);
            }
        })
        )
    }
    })
}
</script>
@endpush