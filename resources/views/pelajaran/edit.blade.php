@extends('layout.app')
@section('title','Edit Data Pelajaran')
@section('content')
<div class="section-heading clearfix">
    <h2>Edit Data Pelajaran</h2>
</div>
<div class="panel-content">
    <form action="/pelajaran/{{$pelajaran->id}}/update" method="post" enctype="multipart/form-data">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="pelajaran">Pelajaran</label>
                        <input type="name" name="pelajaran" class="form-control" id="pelajaran" value="{{$pelajaran->pelajaran}}">
                    </div>
                    <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
            @stop