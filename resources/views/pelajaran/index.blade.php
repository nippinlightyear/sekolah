@extends('layout.app')
@section('title','Data Pelajaran')
@section('Data','active')
@section('Data.Pel','active')
@section('content')

@if(in_array(auth()->user()->role,[1,2]))
<div class="section-heading clearfix">
  <h2 class="section-title"><i class="fa fa-pie-chart"></i> Data Pelajaran </h2>
  <a href="#" class="btn btn-primary btn-sm right" data-toggle="modal" data-target="#exampleModal" ><i class="lnr lnr-plus-circle"></i>Pelajaran</a>
</div>
@endif
<div class="panel-content">
  <table class="table table-hover">
      <tr>
          <th>ID</th>
          <th>Pelajaran</th>
          @if(in_array(auth()->user()->role,[1,2]))
          <th>Option</th>
          @endif
      </tr>
      @foreach($data_pelajaran as $value)
      <tr>
          <td>{{$value->id}}</td>
          <td>{{$value->pelajaran}}</td>
          @if(in_array(auth()->user()->role,[1,2]))
          <td><a href="/pelajaran/{{$value->id}}/edit" class="btn btn-warning btn-xs">
                <span class="sr-only">Update</span>
                <i class="fa fa-pencil"></i>
              </a>
              <a href="/pelajaran/{{$value->id}}/delete" class="btn btn-danger btn-xs">
                <span class="sr-only">Delete</span>
                <i class="fa fa-trash-o"></i>
              </a>
          </td>
          @endif
        </tr>
        @endforeach        
  </table>
</div>

<!-- deletemodal -->
<!-- FORMModal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pelajaran</h5>
            </div>
            <form action="/pelajaran/create" method="post">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="pelajaran">Pelajaran</label>
                        <input type="name" name="pelajaran" class="form-control" id="pelajaran">
                    </div>
                    <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endFORMModal --> 
@stop