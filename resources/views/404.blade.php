<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Page Error 404 | Sekolah Kita</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/linearicons/style.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/metisMenu/metisMenu.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/chartist/css/chartist.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/toastr/toastr.min.css')}}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('admin/assets/css/main.css')}}">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
</head>

<body class="page-error">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<h1>
					<span class="clearfix title">
						<span class="number left">Maaf</span> <span class="text">Mungkin Halalaman Yang <br/>Anda Tuju Tydak Tersedya</span>
					</span>
				</h1>
				<p>Pastikan anda menggunakan akun admin untuk mengakses semua halaman yang tersedia. Please <a href="https://mail.google.com/mail/?view=cm&fs=1&to=nippinlightyear@gmail.com&su=SEKOLAHKITA" target="_blank">contact us</a> to report this issue.</p>
				<div class="margin-top-30">
					<a href="/dashboard" class="btn btn-primary"><i class="fa fa-home"></i> <span>Back</span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>

