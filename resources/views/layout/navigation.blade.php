		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu"></i></button>
				</div>
				<div class="navbar-brand">
					<img src="{{asset('img/SekolahKita-fullHD.png')}}" width="50%" alt="DiffDash Logo" class="img-responsive logo">
				</div>
				<div class="navbar-right">
					<!-- search form -->
					<!-- <form id="navbar-search" class="navbar-form search-form" method="GET" action="/siswa">
						<input name="cari" class="form-control" placeholder="Search here..." type="text">
						<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
					</form> -->
					<!-- end search form -->
					<!-- navbar menu -->
					<div id="navbar-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
									<img src="{{asset('img/SekolahKita-halfHD.png')}}" width="25px" alt="DiffDash Logo" class="img-responsive logo">
								</a>
								<ul class="dropdown-menu user-menu menu-icon">
									<li class="menu-heading">ACCOUNT SETTINGS</li>
									<li><a href="#"><i class="fa fa-fw fa-edit"></i> <span>User Setting</span></a></li>
									<li><a href="/logout"><i class="fa fa-fw fa-lock"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
					<!-- end navbar menu -->
				</div>
			</div>
		</nav>