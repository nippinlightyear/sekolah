<!DOCTYPE html>
<html>
<head>
    <title>Sekolah Kita - @yield('title') </title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/linearicons/style.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/metisMenu/metisMenu.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/datatables/datatables.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/datatables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}">
    
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/chartist/css/chartist.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')}}">
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/toastr/toastr.min.css')}}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('admin/assets/css/main.css')}}">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<!-- <link rel="apple-touch-icon" sizes="76x76" href="admin/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="admin/assets/img/favicon.png"> -->
    @stack('styles')
</head>
<body> 
    <div id="wrapper">
        @include('layout.navigation')
        @include('layout.sidebar')
        <div id="main-content">
            <div class="container-fluid">
                
                    @yield('content')
                
            </div>
        </div>
        <div class="clearfix"></div>
            <footer>
                <p class="copyright">&copy; 2020 <a href="https://www.instagram.com/nippinlightyear_/?hl=id" target="_blank">Soleh Nur Hanifin</a>. All Rights Reserved.</p>
            </footer>
        </div>
    </div>
    <!-- Javascript -->
	<script src="{{asset('admin/assets/vendor/jquery/jquery.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/metisMenu/metisMenu.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/toastr/toastr.js')}}"></script>
    <script src="{{asset('admin/assets/scripts/common.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/datatables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js')}}"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    
    @stack('scripts')
    @show
    
</body>

</html>