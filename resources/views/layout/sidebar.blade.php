		<div id="left-sidebar" class="sidebar">
			<button type="button" class="btn btn-xs btn-link btn-toggle-fullwidth">
				<span class="sr-only">Toggle Fullwidth</span>
				<i class="fa fa-angle-left"></i>
			</button>
			<div class="sidebar-scroll">
				<div class="user-account">
					<img src="{{asset('img/SekolahKita-halfHD.png')}}" width="100px" height="100px" class="img-responsive img-square user-photo" alt="User Profile Picture">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle user-name" data-toggle="dropdown">Hello, <strong>{{auth()->user()->name}}</strong> <i class="fa fa-caret-down"></i></a>
						<ul class="dropdown-menu dropdown-menu-right account">
							<li><a href="#">My Profile</a></li>
							<li><a href="#">Messages</a></li>
							<li><a href="#">Settings</a></li>
							<li class="divider"></li>
							<li><a href="/logout">Logout</a></li>
						</ul>
					</div>
				</div>
				<nav id="left-sidebar-nav" class="sidebar-nav">
					<ul id="main-menu" class="metismenu">
						<li class="@yield('Dashboard')"><a href="/dashboard"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li class="@yield('Data')">
							<a href="#uiElements" class="has-arrow" aria-expanded="false"><i class="lnr lnr-bookmark"></i> <span>Data</span></a>
							<ul aria-expanded="true">
								<li class="@yield('Data.Siswa')"><a href="/siswa">Data Siswa</a></li>
								<li class="@yield('Data.Kelas')"><a href="/kelas">Data Kelas</a></li>
								<li class="@yield('Data.Guru')"><a href="/guru">Data Guru</a></li>
								<li class="@yield('Data.Pel')"><a href="/pelajaran">Data Pelajaran</a></li>
							</ul>
						</li>
						<li class="@yield('Master')">
							<a href="#subPages" class="has-arrow" aria-expanded="false"><i class="lnr lnr-file-empty"></i> <span>Master</span></a>
							<ul aria-expanded="true">
								<li class="@yield('Master.Kelas')"><a href="/kelasmaster">Master Kelas</a></li>
							</ul>
						</li>
						<!--
						<li class="">
							<a href="#forms" class="has-arrow" aria-expanded="false"><i class="lnr lnr-pencil"></i> <span>Forms</span></a>
							<ul aria-expanded="true">
								<li class=""><a href="forms-validation.html">Form Validation</a></li>
								<li class=""><a href="forms-advanced.html">Advanced Form Elements</a></li>
								<li class=""><a href="forms-basic.html">Basic Form Elements</a></li>
								<li class=""><a href="forms-dragdropupload.html">Drag &amp; Drop Upload</a></li>
								<li class=""><a href="forms-texteditor.html">Text Editor</a></li>
							</ul>
						</li>
						<li class="">
							<a href="#charts" class="has-arrow" aria-expanded="false"><i class="lnr lnr-chart-bars"></i> <span>Charts</span></a>
							<ul aria-expanded="true">
								<li class=""><a href="charts-chartist.html">Chartist</a></li>
								<li class=""><a href="charts-sparkline.html">Sparkline Chart</a></li>
							</ul>
						</li>
						<li class=""><a href="notifications.html"><i class="lnr lnr-alarm"></i> <span>Notifications</span> <span class="badge bg-danger">15</span></a></li>
						<li class=""><a href="typography.html"><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li> -->
					</ul>
				</nav>
			</div>
		</div>