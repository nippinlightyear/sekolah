@extends('layout.app')
@section('title','Data Siswa')
@section('content')
				<div class="section-heading">
					<h1 class="page-title">User Profile</h1>
				</div>
				<ul class="nav nav-tabs" role="tablist">
					<li class="active"><a href="#myprofile" role="tab" data-toggle="tab">My Profile</a></li>
					@if(in_array(auth()->user()->id,[1,$siswa->user_id]))
					<li><a href="#account" role="tab" data-toggle="tab">Account</a></li>
					@endif
				</ul>
                <div class="tab-content content-profile">
                    <!-- MY PROFILE -->
						<div class="tab-pane fade in active" id="myprofile">
							<div class="profile-section">
								<h2 class="profile-heading">Profile Photo</h2>
								<div class="media">
									<div class="media-left">
										<img src="{{asset('img/'.$siswa->avatar)}}" width="150px" class="user-photo media-object" alt="User">
									</div>
									<div class="media-body">
										<p>Kelola data diri</p>
										@if(in_array(auth()->user()->role,[1,2]))
										<a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning">
                                            <span class="sr-only">Update</span>
                                            <i class="lnr lnr-pencil"></i>
                                        </a>
										@endif
									</div>
								</div>
							</div>
							<div class="profile-section">
								<h2 class="profile-heading">Data Diri</h2>
								<div class="clearfix">
                                    <div class="left">
                                        <ul class="list-unstyled list-justify">
                                            <li>Nama Lengkap <span>{{$siswa->name}}</span></li>
                                            <li>Jenis Kelamin<span>{{$siswa->jenis_kelamin}}</span></li>
                                            <li>Agama<span>{{$siswa->agama}}</span></li>
                                            <li>Alamat<span>{{$siswa->alamat}}</span></li>
                                        </ul>    
                                    </div>
								</div>
								<div class="right">

                                </div>
							</div>
						</div>
						<!-- END MY PROFILE -->
						<!-- ACCOUNT -->
						<div class="tab-pane fade" id="account">
							<div class="profile-section">
								<h2 class="profile-heading">Acount</h2>
								<div class="clearfix">
                                    <div class="left">
                                        <ul class="list-unstyled list-justify">
											<li>Email<span>{{$user->email}}</span></li>
											<small id="emailHelp" class="form-text text-muted"><strong>Jangan bagikan email ini kepada siapaun</strong> sebelum anda mengganti password</small>
                                        </ul>    
                                    </div>
								</div>
								<div class="right">

                                </div>
							</div>
						</div>
						<!-- END ACCOUNT -->
					</div>
@stop