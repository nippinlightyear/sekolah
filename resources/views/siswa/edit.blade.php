@extends('layout.app')
@section('title','Edit Data Siswa')
@section('content')
<div class="section-heading clearfix">
    <h2>Edit Data Siswa</h2>
</div>
<div class="panel-content">
    <form action="/siswa/{{$siswa->id}}/update" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <div class="profile-section">
                <h2 class="profile-heading">Profile Photo</h2>
                <div class="media">
                    <div class="media-left">
                        <img src="{{asset('img/'.$siswa->avatar)}}" width="150px" class="user-photo media-object" alt="User">
                    </div>
                    <div class="media-body">
                        <p>Ubah photo</p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadmodal">
                        Upload Foto
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="name">Nama Lengkap</label>
                <input type="name" name="name" class="form-control" id="name" value="{{$siswa->name}}">
            </div>
            <div class="form-group">
                <label class="control-lable" for="email">Email</label>
                <p class="form-control-static">{{$user->email}}</p>
                <small id="emailHelp" class="form-text text-muted">Untuk mengganti email silahkan hubungi admin</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                    <option value="L" @if($siswa->jenis_kelamin == "L") selected @endif>Laki-laki</option>
                    <option value="P" @if($siswa->jenis_kelamin == "P") selected @endif>Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="name" name="agama" class="form-control" id="agama" value="{{$siswa->agama}}">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat Lengkap</label>
                <textarea name="alamat" class="form-control" id="alamat" rows="3">{{$siswa->alamat}}</textarea>
            </div>
            <button type="submit" class="btn btn-warning">Update</button>
            <div class="modal fade" id="uploadmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="avatar">Foto</label>
                                <input type="file" name="avatar" id="avatar" class="form-control" value="{{$siswa->avatar}}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>
@stop