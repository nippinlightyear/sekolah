@extends('layout.app')
@section('title','Data Siswa')
@section('Data','active')
@section('Data.Siswa','active')
@section('content')

@if(in_array(auth()->user()->role,[1,2]))
<div class="section-heading clearfix">
  <h2 class="section-title"><i class="fa fa-pie-chart"></i> Data Siswa </h2>
  <a href="#" class="btn btn-primary btn-sm right" data-toggle="modal" data-target="#exampleModal" ><i class="lnr lnr-plus-circle"></i>Siswa</a>
</div>
@endif
<div class="panel-content">
  <table class="table table-hover">
      <tr>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Agama</th>
          <th>Alamat</th>
          @if(in_array(auth()->user()->role,[1,2]))
          <th>Option</th>
          @endif
      </tr>
      @foreach($data_siswa as $value)
      <tr>
          <td><a href="/siswa/{{$value->id}}/profile">{{$value->name}}</a></td>
          <td>{{$value->jenis_kelamin}}</td>
          <td>{{$value->agama}}</td>
          <td>{{$value->alamat}}</td>
          @if(in_array(auth()->user()->role,[1,2]))
          <td><a href="/siswa/{{$value->id}}/edit" class="btn btn-warning btn-xs">
                <span class="sr-only">Update</span>
                <i class="fa fa-pencil"></i>
              </a>
              <a href="#" onclick="deleteSiswa({{$value->id}})" class="btn btn-danger btn-xs">
                <span class="sr-only">Update</span>
                <i class="fa fa-trash-o"></i>
              </a>
          </td>
          @endif
        </tr>
        @endforeach        
  </table>
</div>
<!-- FORMModal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
            </div>
            <form action="/siswa/create" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <input type="hidden" name="role" value="3">
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input type="name" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                        <small id="emailHelp" class="form-text text-muted">Ingat alamat ini untuk login siswa</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="agama">Agama</label>
                        <input type="name" name="agama" class="form-control" id="agama">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat Lengkap</label>
                        <textarea name="alamat" class="form-control" id="alamat" rows="3"></textarea>
                    </div>
                <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endFORMModal --> 
@stop

@push('scripts')
<script>
var done = function(resp){
    window.location.reload();
}
var deleteSiswa = function(id) {
  Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
  if (result.value) {
      $.ajax({
          headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
          url: 'siswa/'+id+'/delete',
          type: 'DELETE',
          data: {id: id},
          dataType: 'JSON',
      })
      Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success',
      $.ajax({
          success: function(resp){
              done(resp);
          }
      })
      )
  }
  })
}
</script>
@endpush