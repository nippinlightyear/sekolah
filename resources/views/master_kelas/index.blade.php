@extends('layout.app')
@section('title', 'Data Kelas')
@section('Master', 'active')
@section('Master.Kelas', 'active')
@section('content')

<div class="section-heading clearfix">
  <h2 class="section-title"><i class="fa fa-pie-chart"></i> Master Kelas </h2>
</div>

<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#kelasTab" role="tab" data-toggle="tab">Kelas</a></li>
    <li><a href="#accountTab" role="tab" data-toggle="tab">Tahun Ajaran</a></li>
</ul>
<div class="tab-content content-profile">
    <div class="tab-pane fade in active" id="kelasTab">
        <div class="profile-section">
            <div class="clearfix">
                <div class="left">
                    <h4 class="profile-heading">Data Kelas</h4>
                    <table class="table table-hover">
                        <tr>
                            <th>Id Kelas</th>
                            <th>Kelas</th>
                            @if(auth()->user()->role == '1')
                            <th>Option</th>
                            @endif
                        </tr>
                        @foreach($data_kelas as $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->kelas}}</td>
                            @if(auth()->user()->role == '1')
                            <td><a href="#" role="modal" onclick="updateKelas({{$value->id}})" class="btn btn-warning btn-xs">
                                    <span class="sr-only" >Update</span>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" role="modal" onclick="deleteKelas({{$value->id}})" class="btn btn-danger btn-xs">
                                    <span class="sr-only">Update</span>
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                            @endif
                            </tr>
                        @endforeach        
                    </table>    
                    <a href="#" role="modal" onclick="tambahKelas()" class="btn btn-primary btn-xs">
                        <span class="sr-only">Tambah</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <div class="right">
                    <h4 class="profile-heading">Data Variable Kelas</h4>
                    <table class="table table-hover">
                        <tr>
                            <th>Id Variable</th>
                            <th>Variable</th>
                            @if(auth()->user()->role == '1')
                            <th>Option</th>
                            @endif
                        </tr>
                        @foreach($data_variable as $variable)
                        <tr>
                            <td>{{$variable->id}}</td>
                            <td>{{$variable->variable}}</td>
                            @if(auth()->user()->role == '1')
                            <td><a href="#" role="modal" onclick="updateVariable({{$variable->id}})" class="btn btn-warning btn-xs">
                                    <span class="sr-only">Update</span>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" role="modal" onclick="deleteVariable({{$variable->id}})" class="btn btn-danger btn-xs">
                                    <span class="sr-only">Delete</span>
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                    <a href="#" role="modal" onclick="tambahVariable()" class="btn btn-primary btn-xs">
                        <span class="sr-only">Tambah</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
            
    </div>
    <div class="tab-pane fade" id="accountTab">
        <div class="profile-section">
            <div class="clearfix">
                <!-- <div class="left"> -->
                    <h4 class="profile-heading">Tahun Ajaran</h4>
                    <table class="table table-hover">
                        <tr>
                            <th>Id Kelas</th>
                            <th>Tahun</th>
                            @if(auth()->user()->role == '1')
                            <th>Option</th>
                            @endif
                        </tr>
                        @foreach($data_tahun as $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->tahun}}</td>
                            @if(auth()->user()->role == '1')
                            <td><a href="#" role="modal" onclick="updateTahun({{$value->id}})" class="btn btn-warning btn-xs">
                                    <span class="sr-only">Update</span>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" role="modal" onclick="deleteTahun({{$value->id}})" class="btn btn-danger btn-xs">
                                    <span class="sr-only">Delete</span>
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach        
                    </table>    
                    <a href="#" role="modal" onclick="tambahTahun()" class="btn btn-primary btn-xs">
                        <span class="sr-only">Tambah</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                <!-- <div class="right">
                    saasdd
                </div> -->
            </div>
        </div>
            
    </div>
</div>

<!-- KumpulanModal -->
    <!-- Modal Kelas -->
<div class="modal fade" id="KelasModal" tabindex="-1" role="dialog" aria-labelledby="KelasModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalKelas">Tambah Data Kelas</h5>
            </div>
            <form id="frmKelas">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Kelas</label>
                        <input type="name" name="kelas" class="form-control" id="kelasinput">
                    </div>
                <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary simpan">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
    <!-- endModal Kelas -->
    <!-- Modal Variable -->
<div class="modal fade" id="VariableModal" tabindex="-1" role="dialog" aria-labelledby="VariableModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalVariable">Tambah Data Kelas</h5>
            </div>
            <form id="frmVariable">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Variable</label>
                        <input type="name" name="variable" class="form-control" id="variable">
                    </div>
                <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary simpanVariable">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
    <!-- endModal Vaariable -->
    <!-- Modal Tahun -->
<div class="modal fade" id="TahunModal" tabindex="-1" role="dialog" aria-labelledby="TahunModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalTahun">Tambah Data Tahun</h5>
            </div>
            <form id="frmTahun">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Tahun</label>
                        <input type="name" name="tahun" class="form-control" id="tahun">
                    </div>
                <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary simpanTahun">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
    <!-- endModal Tahun -->
<!-- endKumpulanModal --> 

@stop
@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>

var formAction, table, uid;
var method = 'POST';

var done = function(resp){
    ModalKelas.modal('hide'),
    ModalVariable.modal('hide'),
    ModalTahun.modal('hide'),
    window.location.reload();
}
// berhubungan kelas
var ModalKelas = $('#KelasModal');
var FormKelas = $('#frmKelas');
var resetFormKelas = function(){
    method = 'POST';
    $('[name="kelas"]').val('');
}
var tambahKelas = function(){
    formAction = '{!! route('create.masterkelas') !!}';
    resetFormKelas();
    ModalKelas.modal('show');
}
var updateKelas = function(id) {
        $.ajax({
            url: 'kelasmaster/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'kelasmaster/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="kelas"]').val(data.kelas);
                console.log(data.kelas)
                ModalKelas.modal('show');
            },
            error: function(resp) {
                ModalKelas.modal('show');
                error(resp);
            }
        })
    }
$(function(){
    $('.simpan').click(function(e){
        e.preventDefault();
        var data = FormKelas.serialize();
        // console.log(data);
        $.ajax({
            url: formAction,
            method: method,
            data: data,
            dataType: 'JSON',
            success: function(resp) {
                    done(resp);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data berhasil disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                },

        });
        return false;
    });
})
var deleteKelas = function(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            url: 'kelasmaster/'+id+'/delete',
            type: 'DELETE',
            data: {id: id},
            dataType: 'JSON',
        })
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success',
        $.ajax({
            success: function(resp){
                done(resp);
            }
        })
        )
    }
    })
}

// variable

var ModalVariable = $('#VariableModal');
var FormVariable = $('#frmVariable');
var resetFormVariable = function(){
    method = 'POST';
    $('[name="variable"]').val('');
}
var tambahVariable = function(){
    formAction = '{!! route('create.variable') !!}'
    resetFormVariable();
    ModalVariable.modal('show');
}
var updateVariable = function(id) {
        $.ajax({
            url: 'variable/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'variable/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="variable"]').val(data.variable);
                // console.log(data.kelas)
                ModalVariable.modal('show');
            },
            error: function(resp) {
                ModalVariable.modal('show');
                error(resp);
            }
        })
    }
$(function(){
    $('.simpanVariable').click(function(e){
        e.preventDefault();
        var data = FormVariable.serialize();
        // console.log(data);
        $.ajax({
            url: formAction,
            method: method,
            data: data,
            dataType: 'JSON',
            success: function(resp) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data berhasil disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    done(resp);
            },

        });
        return false;
    });
})

var deleteVariable = function(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            url: 'variable/'+id+'/delete',
            type: 'DELETE',
            data: {id: id},
            dataType: 'JSON',
        })
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success',
        $.ajax({
            success: function(resp){
                done(resp);
            }
        })
        )
    }
    })
}
// tahun

var ModalTahun = $('#TahunModal');
var FormTahun = $('#frmTahun');
var resetFormTahun = function(){
    method = 'POST';
    $('[name="tahun"]').val('');
}
var tambahTahun = function(){
    formAction = '{!! route('create.tahun') !!}'
    resetFormTahun();
    ModalTahun.modal('show');
}
var updateTahun = function(id) {
        $.ajax({
            url: 'tahun/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'tahun/'+id+'/update';
                method = 'PUT';

                // console.log(resp)
                var data = resp.data;
                $('[name="tahun"]').val(data.tahun);
                ModalTahun.modal('show');
            },
            error: function(resp) {
                ModalTahun.modal('show');
                error(resp);
            }
        })
}
$(function(){
    $('.simpanTahun').click(function(e){
        e.preventDefault();
        var data = FormTahun.serialize();
        // console.log(data);
        $.ajax({
            url: formAction,
            method: method,
            data: data,
            dataType: 'JSON',
            success: function(resp) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data berhasil disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    done(resp);
                },

        });
        return false;
    });
})
var deleteTahun = function(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
        $.ajax({
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            url: 'tahun/'+id+'/delete',
            type: 'DELETE',
            data: {id: id},
            dataType: 'JSON',
        })
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success',
        $.ajax({
            success: function(resp){
                done(resp);
            }
        })
        )
    }
    })
}

</script>
@endpush