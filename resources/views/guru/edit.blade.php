@extends('layout.app')
@section('title','Edit Data Guru')
@section('content')


<div class="section-heading clearfix">
    <h2>Edit Data Guru</h2>
</div>
<div class="panel-content">
    <form action="/guru/{{$guru->id}}/update" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <div class="profile-section">
                <h2 class="profile-heading">Profile Photo</h2>
                <div class="media">
                    <div class="media-left">
                        <img src="{{asset('img/'.$guru->avatar)}}" width="100px" class="user-photo media-object" alt="User">
                    </div>
                    <div class="media-body">
                        <p>Ubah photo</p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadmodal">
                        Upload Foto
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="name">Nama Lengkap</label>
                <input type="name" name="name" class="form-control" id="name" value="{{$guru->name}}">
            </div>
            <div class="form-group">
                <label class="control-lable" for="email">Email</label>
                <p class="form-control-static">{{$user->email}}</p>
                <small id="emailHelp" class="form-text text-muted">Untuk mengganti email silahkan hubungi admin</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                    <option value="L" @if($guru->jenis_kelamin == "L") selected @endif>Laki-laki</option>
                    <option value="P" @if($guru->jenis_kelamin == "P") selected @endif>Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="name" name="agama" class="form-control" id="agama" value="{{$guru->agama}}">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat Lengkap</label>
                <textarea name="alamat" class="form-control" id="alamat" rows="3">{{$guru->alamat}}</textarea>
            </div>
            <button type="submit" class="btn btn-warning">Update</button>
            <!-- modal -->
            <div class="modal fade" id="uploadmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pilih Foto</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="avatar">Foto</label>
                                <input type="file" name="avatar" id="avatar" class="form-control" value="{{$guru->avatar}}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- endmodal -->
    </form>
</div>


@stop