@extends('layout.app')
@section('title','Data Guru')
@section('Data','active')
@section('Data.Guru','active')
@section('content')

<div class="section-heading clearfix">
  <h2 class="section-title"><i class="fa fa-pie-chart"></i> Data Guru </h2>
@if(auth()->user()->role == '1'))
  <a href="#" class="btn btn-primary btn-sm right" data-toggle="modal" data-target="#exampleModal" ><i class="lnr lnr-plus-circle"></i>Guru</a>
@endif
</div>
<div class="panel-content">
  <table class="table table-hover">
      <tr>
          <th>Nama Lengkap</th>
          <th>Jenis Kelamin</th>
          <th>Agama</th>
          <th>Alamat</th>
          @if(auth()->user()->role == '1')
          <th>Option</th>
          @endif
      </tr>

      @foreach($data_guru as $value)
      <tr>
          <td><a href="/guru/{{$value->id}}/profile">{{$value->name}}</a></td>
          <td>{{$value->jenis_kelamin}}</td>
          <td>{{$value->agama}}</td>
          <td>{{$value->alamat}}</td>
          @if(auth()->user()->role == '1')
          <td><a href="/guru/{{$value->id}}/edit" class="btn btn-warning btn-xs">
                <span class="sr-only">Update</span>
                <i class="lnr lnr-pencil"></i>
              </a>
              <a href="#" role="modal" onclick="deleteGuru({{$value->id}})" class="btn btn-danger btn-xs">
                <span class="sr-only">Delete</span>  
                <i class="fa fa-trash-o"></i>
              </a>
          </td>
          @endif
        </tr>
        @endforeach        
  </table>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Guru</h5>
            </div>
            <form action="/guru/create" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <!-- ModalBody -->
                {{csrf_field()}}
                    <input type="hidden" name="role" value="3">
                    <div class="form-group">
                        <label for="name">Nama Lengkap</label>
                        <input type="name" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                        <small id="emailHelp" class="form-text text-muted">Ingat alamat ini untuk login</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="agama">Agama</label>
                        <input type="name" name="agama" class="form-control" id="agama">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat Lengkap</label>
                        <textarea name="alamat" class="form-control" id="alamat" rows="3"></textarea>
                    </div>
                <!-- endModalBody -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endFORMModal --> 
@stop

@push('scripts')
<script>
var done = function(resp){
    window.location.reload();
}
var deleteGuru = function(id) {
  Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
  if (result.value) {
      $.ajax({
          headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
          url: 'guru/'+id+'/delete',
          type: 'DELETE',
          data: {id: id},
          dataType: 'JSON',
      })
      Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success',
      $.ajax({
          success: function(resp){
              done(resp);
          }
      })
      )
  }
  })
}
</script>
@endpush