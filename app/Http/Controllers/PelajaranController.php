<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\PelajaranModel;
class PelajaranController extends Controller
{
    public function index()
    {
       
        $data_pelajaran = PelajaranModel::all();
        return view('pelajaran.index', compact('data_pelajaran'));
    }
    public function create(Request $request)
    {
        // dd($request);
        $pelajaran = PelajaranModel::create($request->all());
        return redirect('/pelajaran')->with('sukses','Data berhasil diinput');
    }
    public function edit($id)
    {
        $pelajaran = PelajaranModel::findOrFail($id);
        return view('pelajaran.edit', compact('pelajaran'));
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $pelajaran = PelajaranModel::findOrFail($id);
        $pelajaran->update($request->all());
        return redirect('/pelajaran')->with('update','Data berhasil di update');
    }
    public function delete($id)
    {
        // dd($id);
        $pelajaran = PelajaranModel::findOrFail($id);
        $pelajaran->delete();
        return redirect('/pelajaran')->with('delete','Data berhasil di hapus');
    }
}