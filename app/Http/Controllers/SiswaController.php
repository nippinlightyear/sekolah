<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\SiswaModel;
use App\User;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_siswa = SiswaModel::where('name', 'LIKE', '%'.$request->cari.'%')->get();
        }else{
            $data_siswa = SiswaModel::all();
        }
        return view('siswa.index', compact('data_siswa'));
    }
    public function create(Request $request)
    {
        $user = new User;
        $user->role = '3';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt('rahasia');
        $user->remember_token = Str::random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id]);
        $siswa = SiswaModel::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('img/', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('sukses','Data berhasil diinput');
    }
    public function edit($id)
    {
        $siswa = SiswaModel::findOrFail($id);
        $user = User::find($siswa->user_id);
        return view('siswa.edit', compact('siswa','user'));
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $siswa = SiswaModel::findOrFail($id);
        $siswa->update($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('img/', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('update','Data berhasil di update');
    }
    public function delete($id)
    {
        // dd($id);
        $siswa = SiswaModel::findOrFail($id);
        $user = User::find($siswa->user_id);
        $siswa->delete();
        $user->delete();
        return redirect('/siswa')->with('delete','Data berhasil di hapus');
    }
    public function profile($id)
    {
        $siswa = SiswaModel::find($id);
        $user = User::find($siswa->user_id);
        return view('siswa.profile', compact('siswa','user'));
    }
}