<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas\Kelas;
use App\Models\Kelas\Variable;
use App\Models\Kelas\TahunPel;
use Response;

class KelasController extends Controller
{
    public function index()
    {
        $data_kelas = Kelas::all();
        $data_variable = Variable::all();
        $data_tahun = TahunPel::all();
        // dd($data_kelas);
        return view('master_kelas.index', compact('data_kelas','data_variable','data_tahun'));
    }
    public function create(Request $request)
    {
        $kelas = Kelas::create($request->all());
        return showResponseSuccess(200, 'Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        // dd($kelas);
        return showResponseSuccess($kelas);
    }
    public function update(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        $kelas->update($request->all());
        return showResponseSuccess(200, 'Data berhasil perbarui');
    }
    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->delete();
        return showResponseSuccess(200, 'Data berhasil ditambahkan');

    }
}
