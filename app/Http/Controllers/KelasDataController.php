<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\KelasModel;
use App\Models\Kelas\Kelas;
use App\Models\Kelas\Variable;
use App\Models\Kelas\TahunPel;

class KelasDataController extends Controller
{
    public function index()
    {
        $master_kelas = Kelas::all();
        $variable = Variable::all();
        $tahunpel = TahunPel::all();
        $data_kelas = KelasModel::all();
        // $data_kelas2 = Kelas::where('id',$data_kelas->kelas_id)->get();
        // dd($data_kelas->kelas->kelas);
        return view('kelas.index',compact('data_kelas','master_kelas','variable','tahunpel'));
    }
    public function create(Request $request)
    {
        // dd($request);
        $kelas = KelasModel::create($request->all());
        return showResponseSuccess(200, 'Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $kelas = KelasModel::find($id);
        return showResponseSuccess($kelas);
    }
    public function update(Request $request, $id)
    {
        $kelas = KelasModel::find($id);
        $kelas->update($request->all());
        return showResponseSuccess(200, 'Data berhasil perbarui');
    }
    public function destroy($id)
    {
        $kelas = KelasModel::findOrFail($id);
        $kelas->delete();
        return showResponseSuccess(200, 'Data berhasil ditambahkan');
    }
}
