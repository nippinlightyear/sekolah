<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\GuruModel;
use App\User;
use Hash;
use Auth;

class GuruController extends Controller
{
    public function index(Request $request)
    {
        // if($request->has('cari')){
        //     $data_guru = GuruModel::where('name', 'LIKE', '%'.$request->cari.'%')->get();
        // }else{
        $data_guru = GuruModel::all();
        return view('guru.index', compact('data_guru'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = new User;
        $user->role = '2';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt('rahasia');
        $user->remember_token = Str::random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id]);
        $guru = GuruModel::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('img/', $request->file('avatar')->getClientOriginalName());
            $guru->avatar = $request->file('avatar')->getClientOriginalName();
            $guru->save();
        }
        return redirect('/guru')->with('sukses','Data berhasil diinput');
    }

    public function edit($id)
    {
        $guru = GuruModel::findOrFail($id);
        $user = User::find($guru->user_id);
        return view('guru.edit', compact('guru','user'));
    }
    public function update(Request $request, $id)
    {
        $guru = GuruModel::findOrFail($id);
        $guru->update($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('img/', $request->file('avatar')->getClientOriginalName());
            $guru->avatar = $request->file('avatar')->getClientOriginalName();
            $guru->save();
        }
        return redirect('/guru')->with('update','Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guru = GuruModel::findOrFail($id);
        $user = User::find($guru->user_id);
        $guru->delete();
        $user->delete();
        return redirect('/guru')->with('delete','Data berhasil di hapus');
    }
    public function profile($id)
    {
        $guru = GuruModel::find($id);
        $user = User::find($guru->user_id);
        // $password = Hash::check(Auth::user()->password,$user->password);
        // dd($password);
        return view('guru.profile', compact('guru','user'));
    }
}
