<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SiswaModel;
use App\Models\GuruModel;
use App\Models\PelajaranModel;
use App\Models\Kelas\Kelas;
use App\Models\Kelas\Variable;

class DashboardController extends Controller
{
    public function index()
    {
        $guru = GuruModel::count();
        $siswa = SiswaModel::count();
        $kelas = Kelas::count();
        $variable = Variable::count();
        $pelajaran = PelajaranModel::count();
        return view('dashboard.index', compact('guru','siswa','kelas','variable','pelajaran'));
    }
}
