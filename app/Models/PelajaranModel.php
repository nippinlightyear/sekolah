<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PelajaranModel extends Model
{
    protected $table = 'table_pelajaran';
    protected $fillable = [
        'id',
        'pelajaran'
    ];
}
