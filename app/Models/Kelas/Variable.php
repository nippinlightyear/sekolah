<?php

namespace App\Models\Kelas;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $table = 'variable';
    protected $fillable = [
        'id',
        'variable'
    ];
    public function dkelas()
    {
        return $this->hasMany('App\Models\KelasModel', 'variable_id');
    }
}
