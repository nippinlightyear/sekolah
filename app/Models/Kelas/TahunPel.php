<?php

namespace App\Models\Kelas;

use Illuminate\Database\Eloquent\Model;

class TahunPel extends Model
{
    protected $table = 'tahun';
    protected $fillable = [
        'id',
        'tahun'
    ];
    public function dkelas()
    {
        return $this->hasMany('App\Models\KelasModel', 'tahun_id');
    }
}
