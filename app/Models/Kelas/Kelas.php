<?php

namespace App\Models\Kelas;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'master_kelas';
    protected $fillable = [
        'id',
        'kelas'
    ];
    public function dkelas()
    {
        return $this->hasMany('App\Models\KelasModel','kelas_id');
    }
}
