<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelasModel extends Model
{
    protected $table = 'kelas';
    protected $fillable = [
        'kelas_id',
        'variable_id',
        'tahun_id'
    ];
    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas\Kelas', 'kelas_id');
    }
    public function variable()
    {
        return $this->belongsTo('App\Models\Kelas\Variable', 'variable_id');
    }
    public function tahunpel()
    {
        return $this->belongsTo('App\Models\Kelas\TahunPel', 'tahun_id');
    }
}
