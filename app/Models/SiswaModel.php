<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiswaModel extends Model
{
    protected $table = 'siswa';
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'jenis_kelamin',
        'agama',
        'alamat',
        'avatar'
    ];
}
