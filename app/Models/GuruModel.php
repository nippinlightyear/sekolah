<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuruModel extends Model
{
    protected $table = 'guru';
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'pelajaran',
        'jenis_kelamin',
        'agama',
        'alamat',
        'avatar'
    ];
}
