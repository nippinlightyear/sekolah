<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('/dashboard');
});

Route::get('/login', 'AuthController@login')->name('login')->middleware('guest');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');
Route::get('/404', function(){
    return view('404');
});

/** RouteGroup Admin+Guru+Siswa */
Route::group(['middleware' => ['auth','checkRole:1,2,3']],function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/guru','GuruController@index');
    Route::get('/guru/{id}/profile', 'GuruController@profile');
    Route::get('/siswa', 'SiswaController@index')->name('indexsiswa');
    Route::get('/siswa/{id}/profile', 'SiswaController@profile');
    Route::get('/pelajaran', 'PelajaranController@index');
    Route::get('/kelas','KelasDataController@index');
});
/** RouteGroup Admin+Guru */
Route::group(['middleware' => ['auth','checkRole:1,2']],function(){
    Route::post('/siswa/create', 'SiswaController@create')->name('create.siswa');
    Route::get('/siswa/{id}/edit', 'SiswaController@edit');
    Route::post('/siswa/{id}/update', 'SiswaController@update');
    Route::delete('/siswa/{id}/delete', 'SiswaController@delete');
    Route::post('/pelajaran/create', 'PelajaranController@create')->name('create.pelajaran');
    Route::get('/pelajaran/{id}/edit', 'PelajaranController@edit');
    Route::post('/pelajaran/{id}/update', 'PelajaranController@update');
    Route::get('/pelajaran/{id}/delete', 'PelajaranController@delete');
    Route::get('/kelasmaster', 'KelasController@index');
});
/** RouteGroup Admin Hungkul */
Route::group(['middleware' => ['auth','checkRole:1']],function(){
    Route::post('/guru/create', 'GuruController@create')->name('create.guru');
    Route::get('/guru/{id}/edit', 'GuruController@edit');
    Route::post('/guru/{id}/update', 'GuruController@update');
    Route::delete('/guru/{id}/delete', 'GuruController@destroy');
    /** Master Kelas */
    Route::post('/kelasmaster/create', 'KelasController@create')->name('create.masterkelas');
    Route::get('/kelasmaster/{id}/edit', 'KelasController@edit');
    Route::put('/kelasmaster/{id}/update', 'KelasController@update');
    Route::delete('/kelasmaster/{id}/delete', 'KelasController@destroy');
    Route::post('/variable/create', 'VariableController@create')->name('create.variable');
    Route::get('/variable/{id}/edit', 'VariableController@edit');
    Route::put('/variable/{id}/update', 'VariableController@update');
    Route::delete('/variable/{id}/delete', 'VariableController@destroy');
    Route::post('/tahun/create', 'TahunController@create')->name('create.tahun');
    Route::get('/tahun/{id}/edit', 'TahunController@edit');
    Route::put('/tahun/{id}/update', 'TahunController@update');
    Route::delete('/tahun/{id}/delete', 'TahunController@destroy');
    /** end Master */
    Route::post('/kelas/create', 'KelasDataController@create')->name('create.kelas');
    Route::get('/kelas/{id}/edit', 'KelasDataController@edit');
    Route::put('/kelas/{id}/update', 'KelasDataController@update');
    Route::delete('/kelas/{id}/delete', 'KelasDataController@destroy');
    
});